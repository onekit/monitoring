<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211129222046 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Default sensor types and devices';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO sensor_type (id, name, measures, code) VALUES (1, 'Температура воздуха', 'C', 'A');");
        $this->addSql("INSERT INTO sensor_type (id, name, measures, code) VALUES (2, 'Температура воды', 'C', 'W');");
        $this->addSql("INSERT INTO sensor_type (id, name, measures, code) VALUES (3, 'Атмосферное давление', 'мм.рт.ст', 'P');");
        $this->addSql("INSERT INTO sensor_type (id, name, measures, code) VALUES (4, 'Влажность воздуха', '%', 'H');");


        $this->addSql("INSERT INTO device (id, name, display_name) VALUES (1, 'abcde', 'Zero');");
        $this->addSql("INSERT INTO device (id, name, display_name) VALUES (2, 'ESPE8DB849932EC', 'test1');");
        $this->addSql("INSERT INTO device (id, name, display_name) VALUES (3, 'E8:DB:84:99:38:F1', 'TestWemos');");
        $this->addSql("INSERT INTO device (id, name, display_name) VALUES (4, '84:CC:A8:B0:73:8C', 'AirMonitor');");
        $this->addSql("INSERT INTO device (id, name, display_name) VALUES (5, '84CCA8B0738C', 'NewMonitor');");
        $this->addSql("INSERT INTO device (id, name, display_name) VALUES (6, '', 'Пустой прибор');");

    }

    public function down(Schema $schema): void
    {
        // Sorry, no way
    }
}
