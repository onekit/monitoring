# What is Monitoring?

This is server-side symfony application for collect weather station sensors data. Using socket or HTTP-GET-request parameters where variable "device" is MAC-address of device and other arguments are sensor names with values as sensorName => sensorValue. 

## In case of running under Docker environment

```
docker-compose up -d
./console.sh
```

Set domain monitoring to 127.0.0.1 in hosts file to test locally 

# Installation

Copy .env.dist to .env and ensure your credentials are correct.

Install composer and run installation:
https://getcomposer.org/download/ 

```
composer install
chmod 755 -R vendor
```

```
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
php bin/console doctrine:migration:execute --up 'DoctrineMigrations\Version20211129222046'
```


# Open port 8888 (optional)

Partially supports NarodMon.com protocol to use Monitoring as backup server.
launch daemon with socket server:

```
./socket-start.sh
```
It will run on 8888 port socket-server for saving sensors data.
To stop server do:
```
./socket-stop.sh
```


# HTTP server

Create HTTP GET request like:
```
GET http://monitoring:80/?device=abcdef&A1=11&W1=22&P1=11&H1=11
Accept: application/json
```

If your sensor name starts with A, W, H or P as P1, W1, H3, P10 it will give measures according to first letter of name:

    A - Air temperature

    W - Water temperature

    H - Humidity

    P - Atmosphere pressure

Mac-address of device will be updated or created. Saved current time.