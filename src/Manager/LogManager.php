<?php

namespace App\Manager;

use App\Repository\DeviceRepository;
use App\Repository\SensorRepository;
use DateTime;
use App\Entity\Log;
use App\Repository\LogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class LogManager
{
    private EntityManagerInterface $em;
    protected LogRepository $logRepository;
    protected DeviceManager $deviceManager;
    protected SensorManager $sensorManager;
    protected RequestStack $requestStack;

    public function __construct(EntityManagerInterface $em, LogRepository $logRepository, DeviceManager $deviceManager, SensorManager $sensorManager, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->logRepository = $logRepository;
        $this->deviceManager = $deviceManager;
        $this->sensorManager = $sensorManager;
        $this->requestStack = $requestStack;
    }

    public function create($device, $sensor, $data): Log
    {
        $time = new DateTime();
        $log = new Log();
        $log->setTime($time);
        $log->setSensor($sensor);
        $log->setDevice($device);
        $log->setData($data);
        $this->update($log, true);

        return $log;
    }

    public function add($data=[]): array
    {
        $request = $this->requestStack->getCurrentRequest();
        $data = $data ?: $request->query->all();
        if (!isset($data['device'])) {
            return ['message'=>'variable device is required'];
        }

        $device = $this->deviceManager->findOneBy(['name'=>$data['device']]) ?: $this->deviceManager->create($data['device']);
        unset($data['device']);
        foreach ($data as $name => $value) {
            $sensor = $device->getSensors()->get($name) ?: $this->sensorManager->create($device, $name, $value);
            $this->create($device, $sensor, $value);
        }

        return ['message'=>'Done'];
    }


    public function update(Log $Log, $flush = false)
    {
        $this->em->persist($Log);
        if ($flush) {
            $this->em->flush();
        }
    }

}

