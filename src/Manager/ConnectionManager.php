<?php

namespace App\Manager;

class ConnectionManager
{

    /** @var LogManager */
    protected LogManager $logManager;

    function __construct(LogManager $logManager)
    {
        $this->logManager = $logManager;
    }

    private function protocolDetect($msg): string
    {
        if (substr($msg, 0, 4) == "#ESP") { // Arduino (ESP)
            return "ESP";
        }

        return 'unknown';
    }

    function sendData($msg, $from)
    {
        $this->{$this->protocolDetect($msg)}($msg, $from);
    }


    function ESP($msg, $from) // NarMon protocol
    {
        echo "ESP Arduino:" . $from->resourceId;
        echo "\n";
        $logData = [];
        $rows = explode("\r\n", $msg);
        $mac = substr($rows[0], 4); //mac address
        if (!$mac) return;
        $logData['device'] = $mac;
        echo "MAC:" . $mac;
        echo "\n";

        array_shift($rows);
        foreach ($rows as $row) {
            $rowData = explode('#',$row);
            $name = $rowData[0];
            $value = $rowData[1];
            $logData[$name] = $value;
            echo "Sensor name:" . $name;
            echo "\n";
            echo "Sensor value:" . $value;
            echo "\n";

        }
        $this->logManager->add($logData);

    }


    function unknown($msg, $from)
    {
        echo "UNKNOWN: ";
        echo $msg; //to debug
        echo "\n";
    }


}

