<?php

namespace App\Manager;

use App\Entity\Sensor;
use App\Repository\SensorRepository;
use App\Repository\SensorTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class SensorManager
{
    private EntityManagerInterface $em;
    protected SensorRepository $sensorRepository;
    protected SensorTypeRepository $sensorTypeRepository;

    public function __construct(EntityManagerInterface $em, SensorRepository $sensorRepository, SensorTypeRepository $sensorTypeRepository)
    {
        $this->em = $em;
        $this->sensorRepository = $sensorRepository;
        $this->sensorTypeRepository = $sensorTypeRepository;
    }


    public function show($id): JsonResponse
    {
        $sensor = $this->find($id);
        if (!$sensor) {
            return new JsonResponse('404', 404);
        }
        return new JsonResponse($this->formatResponse($sensor), 200);
    }

    public function find($id): Sensor
    {
        return $this->sensorRepository->find($id);
    }

    public function findBy($criteria, $orderBy): array
    {
        return $this->sensorRepository->findBy($criteria, $orderBy);
    }


    public function create($device, $name, $value): Sensor
    {
        $code = substr($name, 0, 1);
        $sensorType = $this->sensorTypeRepository->findOneBy(['code'=>$code]);
        $sensorType = $sensorType?: $this->sensorTypeRepository->find(1); // air temperature default
        $sensor = $this->sensorRepository->findOneBy(['name'=>$name, 'device'=>$device]) ?: new Sensor();
        $sensor->setName($name);
        $sensor->setData($value);
        $sensor->setDevice($device);
        $sensor->setSensorType($sensorType);
        $this->update($sensor, true);
        return $sensor;
    }


    public function update(Sensor $sensor, $flush = false)
    {
        $this->em->persist($sensor);
        if ($flush) {
            $this->em->flush();
        }
    }


    public function formatResponse($sensor): array
    {
        if (!$sensor) return [];
        return [
            'id' => $sensor->getId(),
            'name' => $sensor->getName(),
            'device_id' => $sensor->getDevice()->getId(),
            'sensor_type_id' => $sensor->getSensorType()->getId(),
            'data' => $sensor->getData(),
            'logs' => $this->logFormatResponse($sensor->getLogs())
        ];
    }

    public function logFormatResponse($logs) {
        $items = [];
        foreach ($logs as $log) {
            $items[] = [
                'time' => $log->getTime()->format('Y-m-d H:i:s'),
                'data' => $log->getData()
            ];
        }
        return $items;
    }

}

