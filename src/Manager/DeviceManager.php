<?php

namespace App\Manager;

use App\Entity\Device;
use App\Repository\DeviceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

class DeviceManager
{
    private EntityManagerInterface $em;
    protected DeviceRepository $deviceRepository;
    protected RequestStack $requestStack;

    public function __construct(EntityManagerInterface $em, DeviceRepository $deviceRepository, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->deviceRepository = $deviceRepository;
        $this->requestStack = $requestStack;
    }

    public function assignDevice(Device $device, $createDevice): Device
    {
        if (isset($createDevice->name)) {
            $device->setName($createDevice->name);
        }

        if (isset($createDevice->displayName)) {
            $device->setDisplayName($createDevice->displayName);
        }

        return $device;
    }

    /**
     * @param null $device
     * @param null $createDevice
     * @return JsonResponse
     */
    public function updateDevice($device = null, $createDevice = null): JsonResponse
    {
        $device = $device ?: new Device();
        $oldDevice = clone $device;
        $device = $this->assignDevice($device, $createDevice);
        $device = $createDevice ? $this->update($device, $oldDevice) : $this->update($device);

        return new JsonResponse($this->formatResponse($device));
    }

    public function show($id): JsonResponse
    {
        /** @var Device $device */
        $device = $this->find($id);
        if (!$device) {
            return new JsonResponse('404', 404);
        }
        return new JsonResponse($this->formatResponse($device), 200);
    }


    public function find($id): ?Device
    {
        return $this->deviceRepository->find($id);
    }

    public function findBy($criteria, $orderBy): array
    {
        return $this->deviceRepository->findBy($criteria, $orderBy);
    }

    public function findOneBy($criteria): ?Device
    {
        return $this->deviceRepository->findOneBy($criteria);
    }

    public function create($macAddress): Device
    {
        $device = new Device();
        $device->setName($macAddress);
        $device->setDisplayName($macAddress);
        $this->update($device, true);
        return $device;
    }

    public function update(Device $device, $flush = false): Device
    {
        $this->em->persist($device);
        if ($flush) {
            $this->em->flush();
        }

        return $device;
    }

    public function formatResponse($device): array
    {
        if (!$device) return [];
        return [
            'id' => $device->getId(),
            'name' => $device->getName(),
            'display_name' => $device->getDisplayName(),
            'sensors' => $this->sensorFormatResponse($device->getSensors())
        ];
    }

    public function sensorFormatResponse($sensors) {
        $items = [];
        foreach ($sensors as $sensor) {
            $items[] = [
                'id' => $sensor->getId(),
                'name' => $sensor->getName(),
                'data' => $sensor->getData()
            ];
        }
        return $items;
    }

}

