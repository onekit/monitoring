<?php
namespace App\Socket;

use Exception;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use App\Manager\ConnectionManager;
use SplObjectStorage;

class Socket implements MessageComponentInterface
{
    protected SplObjectStorage $clients;
    protected ConnectionManager $manager;

    public function __construct(ConnectionManager $manager)
    {
        $this->clients = new SplObjectStorage;
        $this->manager = $manager;
    }

    public function onOpen(ConnectionInterface $conn)
    {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
        echo "New connection ID: ({$conn->resourceId})\n";
        echo "From IP: {$conn->remoteAddress}\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $this->manager->sendData($msg, $from);
    }

    public function onClose(ConnectionInterface $conn)
    {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        echo "Connection ID: {$conn->resourceId}\n ({$conn->remoteAddress}) has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }


}