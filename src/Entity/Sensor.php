<?php

namespace App\Entity;

use App\Repository\SensorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SensorRepository::class)
 */
class Sensor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="sensors")
     * @ORM\JoinColumn(fieldName="device_id", onDelete="CASCADE")
     */
    private $device;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $data;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $name;


    /**
     * @ORM\ManyToOne(targetEntity="SensorType", inversedBy="sensors")
     */
    private $sensorType;

    /**
     * @ORM\OneToMany(targetEntity="Log", mappedBy="sensor", cascade={"persist","remove"})
     */
    private $logs;

    public function __construct()
    {
        $this->logs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDevice(): ?Device
    {
        return $this->device;
    }

    public function setDevice(Device $device): self
    {
        $this->device = $device;

        return $this;
    }

    public function getData(): ?string
    {
        return $this->data;
    }

    public function setData(string $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSensorType(): ?SensorType
    {
        return $this->sensorType;
    }

    public function setSensorType(SensorType $sensorType): self
    {
        $this->sensorType = $sensorType;

        return $this;
    }

    public function getLogs()
    {
        return $this->logs;
    }

    public function setLogs($logs): void
    {
        $this->logs = $logs;
    }
}
