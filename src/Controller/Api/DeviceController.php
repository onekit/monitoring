<?php

namespace App\Controller\Api;

use App\Entity\Device;
use App\Manager\DeviceManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/device")
 */
class DeviceController extends AbstractFOSRestController
{
    public DeviceManager $deviceManager;

    public function __construct(DeviceManager $deviceManager)
    {
        $this->deviceManager = $deviceManager;
    }

    /**
     * @Rest\Post("", name="device_create")
     * @Rest\View(serializerGroups={"device_get", "device_list", "Default"})
     * @return Device|JsonResponse
     */
    public function deviceCreate(): JsonResponse
    {
        return $this->deviceManager->updateDevice();
    }

    /**
     * @Rest\Get("", name="device_list")
     * @Rest\View(serializerGroups={"device_list"})
     */
    public function deviceList(): JsonResponse
    {
        $data = [];
        foreach ($this->deviceManager->findBy([], ['id'=>'desc']) as $item) {
            $data[] = $this->deviceManager->formatResponse($item);
        }
        return new JsonResponse($data);
    }


    /**
     * show device
     * @Rest\Get("/{id}", name="device_get")
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id = "0"): JsonResponse
    {
        return $this->deviceManager->show($id);
    }



}