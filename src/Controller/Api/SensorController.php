<?php

namespace App\Controller\Api;

use App\Manager\SensorManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sensor")
 */
class SensorController extends AbstractFOSRestController
{
    public SensorManager $sensorManager;

    public function __construct(SensorManager $sensorManager)
    {
        $this->sensorManager = $sensorManager;
    }

    /**
     * @Rest\Get("", name="sensor_list")
     * @Rest\View(serializerGroups={"sensor_list"})
     */
    public function sensorList(): JsonResponse
    {
        $data = [];
        foreach ($this->sensorManager->findBy([], ['id'=>'desc']) as $item) {
            $data[] = $this->sensorManager->formatResponse($item);
        }
        return new JsonResponse($data);
    }

    /**
     * show sensor
     * @Rest\Get("/{id}", name="sensor_get")
     * @param string $id
     * @return JsonResponse
     */
    public function show(string $id = "0"): JsonResponse
    {
        return $this->sensorManager->show($id);
    }

}