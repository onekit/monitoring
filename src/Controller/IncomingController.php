<?php

namespace App\Controller;

use App\Manager\LogManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IncomingController extends AbstractController
{
    /**
     * @Route("/", name="incoming")
     */
    public function index(LogManager $logManager): Response
    {
        return $this->json($logManager->add());
    }
}
