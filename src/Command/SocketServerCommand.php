<?php

namespace App\Command;

use App\Socket\Socket;
use Ratchet\Server\IoServer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SocketServerCommand extends Command
{
    protected static $defaultName = 'app:socket:start';
    public Socket $socket;

    public function __construct(Socket $socket)
    {
        $this->socket = $socket;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:socket:start')
            ->setDescription('Launch socket server for devices on port 8888');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $port = 8888;
        $output->writeln("Starting Socket server");
        $output->writeln("Listening Socket on port " . $port);
        $server = IoServer::factory($this->socket, $port); // Run your app on port 8888
        $server->run();

        return Command::SUCCESS;

    }


}