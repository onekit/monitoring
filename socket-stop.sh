#!/bin/sh
# shellcheck disable=SC2046
# shellcheck disable=SC2009
kill $(ps aux | grep 'socket:start' | awk '{print $2}')
